
export PATH="$PATH:$HOME/.composer/vendor/bin"
export PATH="/usr/local/opt/php@7.2/bin:$PATH"
export PATH="/usr/local/opt/php@7.2/sbin:$PATH"

#Agent Forwarding
ssh-add -K ~/.ssh/id_rsa
alias ssh="ssh -A"

#Restore Default profile when exit session
iterm_profile() {
    # Use the default profile on my local system
    echo -ne "\033]50;SetProfile=Default\a"
    printf "\e]1337;SetBadgeFormat=%s\a" \
           $(echo -n "\(session.hostname)" | base64)
}
# After each command, append to the history file and reread it and set the iterm profile back to local
export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r;
iterm_profile;"

 
# AUTOCOMPLETE FOR SSH CONFIG HOSTS
if [ -f $(brew --prefix)/etc/bash_completion ]; then
. $(brew --prefix)/etc/bash_completion
fi

